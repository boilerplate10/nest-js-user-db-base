import { Body, Controller, Logger, Post, ValidationPipe } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthCredentialDto } from './dto/auth-credential.dto';

@Controller('auth')
export class AuthController {
	private logger = new Logger('AuthController');
	constructor(private authService: AuthService) {}

	@Post('/signup')
	signUp(
		@Body(ValidationPipe) authCredentialDto: AuthCredentialDto,
	): Promise<void> {
		this.logger.verbose('[/auth/signup] - User ' + authCredentialDto.username);
		return this.authService.signUp(authCredentialDto);
	}

	@Post('/signin')
	signIn(
		@Body(ValidationPipe) authCredentialDto: AuthCredentialDto,
	): Promise<{ accessToken: string }> {
		this.logger.verbose('[/auth/signin] - User ' + authCredentialDto.username);
		return this.authService.signIn(authCredentialDto);
	}
}
