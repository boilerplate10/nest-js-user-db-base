import {
	ConflictException,
	InternalServerErrorException,
	Logger,
} from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { AuthCredentialDto } from './dto/auth-credential.dto';
import { User } from './user.entity';
import rolesEnum from '../enums/roles.enum';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
	private logger = new Logger('UserRepository');

	async signUp(authCredentialDto: AuthCredentialDto): Promise<void> {
		const { username, password } = authCredentialDto;

		// Generate an unique salt to the user password
		const salt = await bcrypt.genSalt();

		const user = new User();
		user.username = username;
		user.salt = salt;
		user.role = rolesEnum.USER;
		user.password = await this.hashPassword(password, salt);

		try {
			await user.save();
			this.logger.verbose(`User "${user.username}" successfully created.`);
		} catch (error) {
			this.logger.error(
				`Failed to create user "${user.username}"`,
				error.stack,
			);
			// 23505 - Duplicated user
			if (error.code === '23505') {
				throw new ConflictException('Username already exists');
			} else {
				throw new InternalServerErrorException(error);
			}
		}
	}

	async validateUserPassword(
		authCredentialDto: AuthCredentialDto,
	): Promise<string> {
		const { username, password } = authCredentialDto;

		const user = await this.findOne({ username });

		if (user && (await user.validatePassword(password))) {
			return user.username;
		}

		this.logger.warn(`User "${username}" not found or password is invalid.`);

		return null;
	}

	private async hashPassword(password: string, salt: string): Promise<string> {
		return bcrypt.hash(password, salt);
	}
}
